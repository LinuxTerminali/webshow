package com.terminali.webseries;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class About extends Fragment {
    private String name = "";
    private String tvf = "TVF-ONE : India's Premium Online Entertainment Network, owned and operated by 'The Viral Fever Group'. \n" +
            "\n" +
            "TVF-ONE is India's first and biggest Organized OnlineTV, where you can watch regular shows and content, created especially for the young audience, across genres like Humor/Drama/Trends etc. on its channels 'TVF-Qtiyapa' & 'TVF-RecycleBin' \n" +
            "The Viral Fever or (TVF), Founded by Arunabh Kumar, is a new age Media Brand, driven by the spirit of \"Lights...Camera...Experiment !\" with businesses in Branded Content, Live Events, TV Shows, Production Services and Youth Entertainment. \n" +
            "\n" +
            "SUBSCRIBE for your weekly dose of Humor and Qtiyapa!";

    private String yrf = "Y-Films is the dynamic, vibrant start-up at the intersection of films, creativity" +
            " and youth culture that hopes to challenge the norm and detonate boundaries. " +
            "Giving the youth a creative outlet and voice that will entertain and unite on" +
            " film and beyond. A studio that promises to deliver kick ass content of the youth," +
            " by the youth, for the youth. And be a platform for talent - both on screen and " +
            "behind the scenes - that will break and set new rules of story-telling. " +
            "Wanna know more? Come on in… www.yfilms.in";

   private String arre = "Arré brings you engaging videos, web series (fiction and non-fiction)," +
            " investigative documentaries, audio series, doodles, illustrations and more. " +
            "Arré is new-age infotainment. Arré is change. Arré is a conversation. " +
            "Let's talk. http://www.arre.co.in/profile/welcome-to-arre-arre-originals-trailer/";

    private String sony = "Sony LIV is the first premium Video on Demand (VOD) service by Sony Pictures Networks India Private Limited (SPN), providing multi-screen engagement to users on all devices.\n" +
            "Launched in January 2013, it enables users to discover 20 years of rich content from the network channels of Sony Pictures Networks India Private Limited We also provide a rich array of Movies, strong line-up of events across all Sports, TV Shows and Music, product reviews.\n" +
            "With 20 million app downloads so far, LIV is the first amongst its competition to provide original exclusive premium content.\n" +
            "As a true pioneer in its space, Sony LIV launched India’s first-ever original show exclusively for the online platform earlier this year. With #LoveBytes, it became the country’s first digital video-on-demand (VOD) platform to introduce an innovation of this nature.";
    private String scoop = "Welcome to the official YouTube channel of ScoopWhoop. We create videos because we are in love with them. Whatever instigates, encapsulates and entertains, we will make sure to present them to you in their most vibrant form. \n" +
            "\n" +
            "If you want to share ideas and get mentioned in the videos, mail us at - video@scoopwhoop.com |";

    private String dice = "Dice Media is the premium digital video division of Pocket Aces, where we create video properties that will be loved and followed by a vast, young audience. We also run the \"Dice Creator Network\", where we support young creators who want to make video content but don't have the resources to do so.";

    private String tbm = "TBM Storytellers : Digital Cinema Network | India's popular Short film channel. We believe telling stories that were never meant to be told. Talking Books Movies is a group of storytellers who share a common love for telling great stories to the world. Comedy, Thriller, Dark Humour, Suspense, Romance, Drama, all shades of human feelings play at one click. Passionate music, narration, deft camerawork and editing come together to capture each beautiful story in the span of few unforgettable minutes.\n";

    private  String talkies = "Indian comedy shows, inspiring web-series, aur music ke liye dekho Web Talkies. We serve mazedaar entertainment in Hinglish and other Indian languages for Indians around the world. The channel is available on Youtube, Facebook and www.webtalkies.in. Our app will be launching soon. The platform has been conceived and promoted by former head of fiction programming at Sony Entertainment Television Mr. Virendra Shahaney and backed by Mr. Kundan Dhake of Siddhivinayak group.\n" +
            "\n" +
            "Web Talkies will soon be available for family viewing on smart TVs. With its wide range of offerings like original content in fiction and non-fiction space, Internationally successful licensed content, Indie content and AFPs, Web Talkies aims to emerge as the destination of choice for the generation of global Indians. You can contact us on: +912266974232";
    private String old = "Popular 90 Shows are back on digital platform. Use Feedback form to suggest more such shows";

    private String other = "Shows from Emerging publishers like Bindass, put chutney , BLUSH, Rascals, Timeliners and VDM Studio";
    public About() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            name = bundle.getString("key");
            Log.d("name", name);
        }
        super.onCreate(savedInstanceState);
    }
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.about, container, false);
            TextView textView = (TextView) view.findViewById(R.id.text);

            if (name.equals("tvf")) {
                textView.setText(tvf);
            } else if (name.equals("yrf")) {
                textView.setText(yrf);
            } else if (name.equals("arre")) {
                textView.setText(arre);
            } else if (name.equals("sony")) {
                textView.setText(sony);
            } else if (name.equals("scoop")) {
                textView.setText(scoop);
            } else if (name.equals("dice")) {
                textView.setText(dice);
            } else if (name.equals("tbm")) {
                textView.setText(tbm);
            } else if (name.equals("talkies")) {
                textView.setText(talkies);
            } else if (name.equals("old")) {
                textView.setText(old);
            } else if (name.equals("other")) {
                textView.setText(other);
            }
        }
            return view;
        }
}
