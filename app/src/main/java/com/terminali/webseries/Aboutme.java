package com.terminali.webseries;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Aboutme extends Fragment {

    public Aboutme() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("About Webshow");
        View view = inflater.inflate(R.layout.fragment_aboutme, container, false);
        TextView textView = (TextView) view.findViewById(R.id.text);
        TextView textView1 = (TextView) view.findViewById(R.id.credit1);
        TextView textView2 = (TextView) view.findViewById(R.id.credit2);
        String htmlText = "<body><p>Webshow doesn't host or upload any shows. " +
                "Webshow simply aggregates youtube playlist in a convenient, user-friendly interface." +
                "All copyright of Shows and videos is reserved by the copyright owner(s)." +
                "If you like to remove your show or like to suggest a show please use Feedback form." +
                "</p></body>";
        textView.setText(Html.fromHtml(htmlText));
        // stringBuilder.append(Html.fromHtml("<strong>Picasso</strong> "));
        String c = String.valueOf(Html.fromHtml("<small>version:2.2.0 copyright © 2016 Square</small>"));
        textView1.setText(c);
        //stringBuilder1.append(Html.fromHtml("<strong>loopj</strong> "));
        textView2.setText(String.valueOf(Html.fromHtml("<strong>version 4.3.6 copyright © 2016 loopj </strong> ")));
        return view;
    }

}
