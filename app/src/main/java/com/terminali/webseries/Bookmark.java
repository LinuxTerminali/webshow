package com.terminali.webseries;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.List;

import static com.terminali.webseries.Youtube.video_image;

/**
 * A simple {@link Fragment} subclass.
 */
public class Bookmark extends Fragment {


    public Bookmark() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("saved",video_image.toString());
        final List<Datab> list= Datab.listAll(Datab.class);
        View view = inflater.inflate(R.layout.bookmark_fragment, container, false);
        TextView textView = (TextView) view.findViewById(R.id.t);
        GridView gridView = (GridView) view.findViewById(R.id.grid);
        getActivity().setTitle("Bookmark");

        if(list.size()>0) {
            textView.setVisibility(View.INVISIBLE);
            gridView.setAdapter(new BookmarkAdapeter(getActivity(), list));
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getActivity(), Youtube.class);
                    Bundle extras = new Bundle();
                    Datab datab = list.get(position);
                    extras.putString("id", datab.video_id);
                    extras.putString("name", datab.image_name);
                    extras.putString("image_url", datab.video_image);
                    i.putExtras(extras);
                    startActivity(i);
                }


            });
        }else{
            gridView.setVisibility(View.INVISIBLE);
            textView.setText("No Bookmark added yet");
        }
        return view;

    }

}
