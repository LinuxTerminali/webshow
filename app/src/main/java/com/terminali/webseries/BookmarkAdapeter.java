package com.terminali.webseries;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;



public class BookmarkAdapeter extends BaseAdapter {
    private final Context mContext;
    private final List<Datab> list ;


    public BookmarkAdapeter(Context c, List<Datab> list) {
        mContext = c;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (convertView==null) {
            convertView = inflater.inflate(R.layout.bookmark_adapter, parent, false);
        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.poster_image);
        TextView textView = (TextView) convertView.findViewById(R.id.name);
        Datab datab = list.get(position);
        textView.setText(datab.image_name);
        /*imageView.setImageResource(mThumb[position]);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(100, 100));*/

        //String val = String.valueOf(imageView.getTag());
        Log.d("poster", datab.image_name);
        Picasso.with(mContext).load(datab.video_image).into(imageView);

        return convertView;
    }
}

