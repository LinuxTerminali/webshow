package com.terminali.webseries;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Buyme extends Fragment {

    public Buyme() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Buy me a Coffee");
        View view = inflater.inflate(R.layout.fragment_buyme, container, false);
        TextView textView = (TextView) view.findViewById(R.id.text);
        String htmlText = "<body><p>If you liked WebShow and want to support " +
                "the developer then try shopping online using our Affiliate links of <a href=\"http://dl.flipkart.com/dl/?affid=nkmishra1\">" +
                "Flipkart<a> and <a href=\"http://amzn.to/2g7gt68\"> Amazon<a> or use <a href=\"http://paypal.me/NikhilMishra\">Paypal <a>." +
                "</p></body>";
        textView.setText(Html.fromHtml(htmlText));
        textView.setMovementMethod(LinkMovementMethod.getInstance());



        return view;
    }

}
