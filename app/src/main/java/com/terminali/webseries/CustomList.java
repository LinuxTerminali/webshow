package com.terminali.webseries;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class CustomList extends RecyclerView.Adapter<CustomList.ViewHolder> {
    private final ArrayList<Modals> modals;
    private final Context context;
    private int count = 0;


    public CustomList(Context context, ArrayList<Modals> modals) {
        this.modals = modals;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_single, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Modals results = modals.get(position);

        final String actualurl = results.getImage_url();
        //Log.d("actualurl", modals.size()+"");
        final String title = results.getTitle();
        Picasso.with(context).load(actualurl).error(R.drawable.default_film).resize(600, 600).into(holder.imageView);
        holder.textView.setText(title);
        //holder.linearLayout.setBackgroundResource(R.drawable.default_film);


        holder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                Boolean j = preferences.getBoolean("youtube_player",false);
                if(j){
                    String url = "https://www.youtube.com/watch?v="+results.getVideoId();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                }else {


                    Log.d("tag,", results.getVideoId() + position);
                    Intent i = new Intent(context, Youtube.class);
                    Bundle extras = new Bundle();
                    extras.putString("id", results.getVideoId());
                    extras.putString("name", title);
                    extras.putString("image_url", actualurl);
                    i.putExtras(extras);
                    context.startActivity(i);
                }

            }
        });
        holder.disc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 1) {
                    holder.textDisc.setVisibility(View.GONE);
                    count--;
                } else {
                    holder.textDisc.setVisibility(View.VISIBLE);
                    holder.textDisc.setText(results.getDiscription());
                    count++;
                }


                //context.startActivity(i);


            }
        });

    }

    @Override
    public int getItemCount() {
        return modals.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView imageView;
        public final TextView textView;
        public final TextView textDisc;
        public final Button disc;
        public final Button text;
        //LinearLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            //linearLayout =(LinearLayout) itemView.findViewById(R.id.linear);
            imageView = (ImageView) itemView.findViewById(R.id.im);
            textView = (TextView) itemView.findViewById(R.id.text);
            disc = (Button) itemView.findViewById(R.id.discription);
            textDisc = (TextView) itemView.findViewById(R.id.textSpacerNoButtons);
            textDisc.setVisibility(View.GONE);
            disc.setText("Info");
            text = (Button) itemView.findViewById(R.id.button);
            text.setText("Watch Now");
            //text.setVisibility(View.INVISIBLE);

        }
    }
   /* @Override
    public View getView(int position, View view, ViewGroup parent) {
        Modals results = getItem(position);
        String actualurl= results.getImage_url();
        if(view==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view= inflater.inflate(R.layout.list_single,parent,false);
        }


        ImageView imageView = (ImageView) view.findViewById(R.id.im);
        //txtTitle.setText(web[position]);

        Picasso.with(getContext()).load(actualurl).error(R.drawable.permanent).into(imageView);
        return view;
    }*/
}
