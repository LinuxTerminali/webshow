package com.terminali.webseries;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by TERMINALi on 11/26/2016.
 */

public class CustomPagerAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<String> slider_image=  new ArrayList<String>();
    ArrayList<String> title =  new ArrayList<>();
    ArrayList<String> trailer = new ArrayList<>();
    ArrayList<String> playid = new ArrayList<>();
    ArrayList<String> id2 = new ArrayList<>();


    public CustomPagerAdapter(Context context, ArrayList<String> im, ArrayList<String> title,
                              ArrayList<String> trailer,ArrayList<String> playid,ArrayList<String> id2 ) {
        mContext = context;
        this.slider_image= im;
        this.title = title;
        this.trailer = trailer;
        this.playid= playid;
        this.id2 = id2;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return slider_image.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        TextView textView =  (TextView) itemView.findViewById(R.id.text);
        textView.setText(title.get(position));
        Picasso.with(mContext).load(slider_image.get(position)).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext,Series_detail.class);
                Bundle extras = new Bundle();
                extras.putString("playlist",playid.get(position));
                extras.putString("name",title.get(position));
                extras.putString("trailer",trailer.get(position));
                extras.putString("second_id",id2.get(position));
                i.putExtras(extras);
                //Toast.makeText(getActivity().this,"Clicked at"+type,Toast.LENGTH_SHORT).show();
                mContext.startActivity(i);

            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}

