package com.terminali.webseries;

import com.orm.SugarRecord;


public class Datab extends SugarRecord {
    String video_id;
    String image_name;
    String video_image;

    public Datab(){

    }
    public  Datab(String video_id, String image_name,String video_image){
        this.video_id=video_id;
        this.image_name=image_name;
        this.video_image=video_image;
    }
}
