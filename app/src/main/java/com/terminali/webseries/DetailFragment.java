package com.terminali.webseries;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBarUtils;
import fr.castorflex.android.smoothprogressbar.SmoothProgressDrawable;


public class DetailFragment extends Fragment {

    private FetchClient client;
    private CustomList customList;
    private RecyclerView listView;
    private ArrayList<Modals> searchResults;
    private String playid = "";
    private String name="";
    private  String trailer = "";
    private String image_url="";
    private String second_id="";
    SmoothProgressBar progressBar;
    View view;

    public DetailFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        if(bundle!=null){
          playid  = bundle.getString("playid");
            Log.d("play",playid);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(view == null) {
            view = inflater.inflate(R.layout.detail_fragment, container, false);
            fetch(playid);
        }
        progressBar = (SmoothProgressBar) view.findViewById(R.id.pocket);
        progressBar.setSmoothProgressDrawableBackgroundDrawable(
                SmoothProgressBarUtils.generateDrawableWithColors(
                        getResources().getIntArray(R.array.pocket_background_colors),
                        ((SmoothProgressDrawable) progressBar.getIndeterminateDrawable()).getStrokeWidth()));
        progressBar.progressiveStart();
        progressBar.setVisibility(View.VISIBLE);
        Log.d("wewere ", "inside");
        //fetch(playid);
        listView = (RecyclerView) view.findViewById(R.id.list);
        listView.setHasFixedSize(true);
        listView.setVisibility(View.INVISIBLE);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        searchResults = new ArrayList<Modals>();
        customList = new CustomList(getActivity(), searchResults);
        listView.setLayoutManager(layoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        Log.d("setting", "adapter");
        listView.setAdapter(customList);
        Log.d("setted", "adp");
        return view;
    }


    private void fetch(String playlist_id) {
        client = new FetchClient();

        client.getVideos(playlist_id, new JsonHttpResponseHandler() {
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progressBar.setVisibility(View.VISIBLE);
            }
            @Override
            public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                //progressBar.setVisibility(View.VISIBLE);

            }
            public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                //progressBar.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                JSONArray item = null;

                try {
                    item = responseBody.getJSONArray("items");
                    //int values = responseBody.getInt("totalResults");
                    //video_id = new String[values];
                    //image_url = new String[values];

                    if (item.length() == 0) {
                        Toast.makeText(getActivity(), "Network error!", Toast.LENGTH_SHORT).show();
                    } else {
                        //searchResults = Modals.fromJson(item);
                        // Load model objects into the adapter
                        for (int i = 0; i < item.length(); i++) {
                            responseBody = item.getJSONObject(i);
                            String image_url;
                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                            Boolean j = preferences.getBoolean("image_res",false);
                            if(j) {
                                try {
                                    image_url = responseBody.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("maxres").getString("url");

                                } catch (Exception e) {
                                    image_url = responseBody.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("high").getString("url");

                                }
                            }else{
                                image_url = responseBody.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("high").getString("url");
                            }Log.d("image_url", image_url);
                            String videoId = responseBody.getJSONObject("snippet").getJSONObject("resourceId").getString("videoId");
                            String title = responseBody.getJSONObject("snippet").getString("title");
                            String Discription = responseBody.getJSONObject("snippet").getString("description");
                            Modals movie = new Modals(image_url, videoId, title, Discription);
                            searchResults.add(movie);

                        }
                        Log.d("items", item.length() + "of");

                        customList.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    Log.v("LOG_TAG", "id", e);
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.INVISIBLE);
                listView.setVisibility(View.VISIBLE);
            }
        });
    }

}
