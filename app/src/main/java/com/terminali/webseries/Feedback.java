package com.terminali.webseries;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;


/**
 * A simple {@link Fragment} subclass.
 */
public class Feedback extends Fragment {


    public Feedback() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_feedback, container, false);
        // Inflate the layout for this fragment

        final EditText feedbackField = (EditText) view.findViewById(R.id.EditTextFeedbackBody);

        final Spinner feedbackSpinner = (Spinner) view.findViewById(R.id.SpinnerFeedbackType);

        final CheckBox responseCheckbox = (CheckBox) view.findViewById(R.id.CheckBoxResponse);
        final boolean bRequiresResponse = responseCheckbox.isChecked();
        Button mbutton = (Button) view.findViewById(R.id.ButtonSendFeedback);
        mbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String feedback = feedbackField.getText().toString()+" Response Required: " +bRequiresResponse;
                final String feedbackType = feedbackSpinner.getSelectedItem().toString();
                String to ="nkmishra1995@yahoo.in";
                Intent mEmail = new Intent(Intent.ACTION_SEND);
                mEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
                mEmail.putExtra(Intent.EXTRA_SUBJECT, feedbackType);
                mEmail.putExtra(Intent.EXTRA_TEXT, feedback);

                // prompts to choose email client
                mEmail.setType("message/rfc822");

                startActivity(Intent.createChooser(mEmail, "Choose an email client to send your feedback!"));

            }
        });
        return view;
    }

}
