package com.terminali.webseries;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;



public class FetchClient {
    private AsyncHttpClient client;

    public FetchClient() {

        this.client = new AsyncHttpClient();
    }

    private String getAPIurl(String relativeUrl) {
        String API_base_URL = "https://www.googleapis.com/youtube/v3/";
        return API_base_URL + relativeUrl;
    }

    public void getVideos(String playlist_id, JsonHttpResponseHandler handler) {
        String url = getAPIurl("playlistItems?part=snippet");
        RequestParams params = new RequestParams();
        //Set<Integer> list = new HashSet<Integer>();
        params.put("maxResults", 50);
        params.put("playlistId", playlist_id);
        String API_key = "AIzaSyCk_HAQ_PChKmTCzWFS8JCHvpre6Gt94UM";
        params.put("key", API_key);
        client.get(url, params, handler);
        //Log.d("complete_url",toString(client.get(url,params,handler)));

    }


}

