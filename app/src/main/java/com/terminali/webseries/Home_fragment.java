package com.terminali.webseries;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBarUtils;
import fr.castorflex.android.smoothprogressbar.SmoothProgressDrawable;
import me.relex.circleindicator.CircleIndicator;

import static com.terminali.webseries.R.id.pager;


public class Home_fragment extends Fragment {
   ArrayList<String>  slider_image=  new ArrayList<String>();
    ArrayList<String> slider_title = new ArrayList<String>();
    ArrayList<String> slider_id  =  new ArrayList<String>();
    ArrayList<String> slider_trailer = new ArrayList<String>();
    ArrayList<String> slider_id2= new ArrayList<>();
    Button retry_button;


    TwoWayView other_list;
    PosterAdapter other_Poster ;
    ArrayList<String> other_image = new ArrayList<String>();
    ArrayList<String> other_title = new ArrayList<String>();
    ArrayList<String> other_id = new ArrayList<String>();
    ArrayList<String> other_trailer = new ArrayList<String>();
    ArrayList<String> other_id2 = new ArrayList<String>();
    Button other_button;
    TextView other_text;

    TwoWayView popular_list;
    PosterAdapter popular_Poster ;
    ArrayList<String> popular_image = new ArrayList<String>();
    ArrayList<String> popular_title = new ArrayList<String>();
    ArrayList<String> popular_id = new ArrayList<String>();
    ArrayList<String> popular_trailer = new ArrayList<String>();
    ArrayList<String> popular_id2 = new ArrayList<>();
    Button popular_button;
    TextView popular_text;

    TwoWayView old_list;
    PosterAdapter old_Poster ;
    ArrayList<String> old_image = new ArrayList<String>();
    ArrayList<String> old_title = new ArrayList<String>();
    ArrayList<String> old_id = new ArrayList<String>();
    ArrayList<String> old_trailer = new ArrayList<String>();
    ArrayList<String> old_id2 = new ArrayList<>();
    Button old_button;
    TextView old_text;

    FrameLayout fragment;


     SmoothProgressBar progressBar;
     Boolean running = false;
    WebClient client;
    private Handler handler;
    ProgressDialog progressDialog;
    private int delay = 8000; //milliseconds

    private int page = 0;
    ViewPager mViewPager;
    CustomPagerAdapter mCustomPagerAdapter;
    Runnable runnable = new Runnable() {
        public void run() {
            if (mCustomPagerAdapter.getCount() == page) {
                page = 0;
            } else {
                page++;
            }
            mViewPager.setCurrentItem(page, true);
            handler = new Handler();
            handler.postDelayed(this, delay);
        }
    };

    public Home_fragment() {

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Webshow");
        if (view == null) {
            view = inflater.inflate(R.layout.home_fragment, container, false);
            old_pub("old");
            other_pub("other");
            popular_pub("popular");
            fetchSlider("latest");



            mCustomPagerAdapter = new CustomPagerAdapter(getActivity(), slider_image, slider_title, slider_trailer, slider_id, slider_id2);
            retry_button = (Button) view.findViewById(R.id.retry);
            retry_button.setVisibility(View.INVISIBLE);

            other_text = (TextView) view.findViewById(R.id.textView2);
            other_text.setVisibility(View.INVISIBLE);

            popular_text = (TextView) view.findViewById(R.id.textView3);
            popular_text.setVisibility(View.INVISIBLE);


            old_text = (TextView) view.findViewById(R.id.old_text);
            old_text.setVisibility(View.INVISIBLE);

            progressBar = (SmoothProgressBar) view.findViewById(R.id.pocket);
            progressBar.setSmoothProgressDrawableBackgroundDrawable(
                    SmoothProgressBarUtils.generateDrawableWithColors(
                            getResources().getIntArray(R.array.pocket_background_colors),
                            ((SmoothProgressDrawable) progressBar.getIndeterminateDrawable()).getStrokeWidth()));
            progressBar.progressiveStart();
            progressBar.setVisibility(View.VISIBLE);

            //running = false;

            CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator);
            mViewPager = (ViewPager) view.findViewById(pager);
            mViewPager.setClipToPadding(true);
            mViewPager.setPadding(16, 10, 16, 0);
            mViewPager.setAdapter(mCustomPagerAdapter);
            indicator.setViewPager(mViewPager);
            //fetchSlider("latest");
            mCustomPagerAdapter.registerDataSetObserver(indicator.getDataSetObserver());
            runnable.run();
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    page = position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            other_button = (Button) view.findViewById(R.id.button2);
            other_button.setText("Explore");
            other_button.setVisibility(View.INVISIBLE);
            other_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment childFragment = new PublishersFrag();
                    Bundle bundle = new Bundle();
                    bundle.putString("key","other");
                    childFragment.setArguments(bundle);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack("other");
                    transaction.replace(R.id.child_fragment_container, childFragment, "other").commit();

                }
            });

            //other_pub("other");
            other_list = (TwoWayView) view.findViewById(R.id.list);
            other_Poster = new PosterAdapter(getActivity(), other_image, other_title);
            other_list.setAdapter(other_Poster);
            other_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getActivity(), Series_detail.class);
                    Bundle extras = new Bundle();
                    extras.putString("playlist", other_id.get(position));
                    extras.putString("name", other_title.get(position));
                    extras.putString("trailer", other_trailer.get(position));
                    extras.putString("image_url", other_image.get(position));
                    extras.putString("second_id", other_id2.get(position));
                    i.putExtras(extras);
                    //Toast.makeText(getActivity().this,"Clicked at"+type,Toast.LENGTH_SHORT).show();
                    startActivity(i);
                }
            });
            //popular_pub("popular");
            //progressBar.setVisibility(View.GONE);

            popular_button = (Button) view.findViewById(R.id.button);
            popular_button.setText("More");
            popular_button.setVisibility(View.INVISIBLE);
            popular_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment childFragment = new PopularFrag();
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack("popular");
                    transaction.replace(R.id.child_fragment_container, childFragment, "popular").commit();

                }
            });
            popular_list = (TwoWayView) view.findViewById(R.id.list2);
            popular_Poster = new PosterAdapter(getActivity(), popular_image, popular_title);
            popular_list.setAdapter(popular_Poster);
            popular_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getActivity(), Series_detail.class);
                    Bundle extras = new Bundle();
                    extras.putString("playlist", popular_id.get(position));
                    extras.putString("name", popular_title.get(position));
                    extras.putString("trailer", popular_trailer.get(position));
                    extras.putString("image_url", popular_image.get(position));
                    extras.putString("second_id", popular_id2.get(position));
                    i.putExtras(extras);
                    //Toast.makeText(getActivity().this,"Clicked at"+type,Toast.LENGTH_SHORT).show();
                    startActivity(i);

                }
            });
            //old_pub("old");
            old_button = (Button) view.findViewById(R.id.old_button);
            old_button.setText("More");
            old_button.setVisibility(View.INVISIBLE);
            old_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment childFragment = new PublishersFrag();
                    Bundle bundle = new Bundle();
                    bundle.putString("key","old");
                    childFragment.setArguments(bundle);
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack("old");
                    transaction.add(R.id.child_fragment_container, childFragment, "old").commit();

                }
            });
            old_list = (TwoWayView) view.findViewById(R.id.old_list);
            old_Poster = new PosterAdapter(getActivity(), old_image, old_title);
            old_list.setAdapter(old_Poster);
            old_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getActivity(), Series_detail.class);
                    Bundle extras = new Bundle();
                    extras.putString("playlist", old_id.get(position));
                    extras.putString("name", old_title.get(position));
                    extras.putString("trailer", old_trailer.get(position));
                    extras.putString("image_url", old_image.get(position));
                    extras.putString("second_id", old_id2.get(position));
                    i.putExtras(extras);
                    startActivity(i);
                }
            });
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        if (view.getParent() != null) {
            ((ViewGroup)view.getParent()).removeView(view);
        }
        super.onDestroyView();
    }

  /*  @Override
    public  void onPause(){
        Log.e("DEBUG", "onpause of LoginFragment");
        int count = getChildFragmentManager().getBackStackEntryCount();
        Log.d("count",count+"");
        super.onPause();
        //fragment.setVisibility(View.INVISIBLE);
        //getChildFragmentManager().popBackStack();
        Fragment childFragment = new Home_fragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.child_fragment_container, childFragment).commit();

    }*/

    @Override
    public  void onStart(){
        Log.e("DEBUG", "onstart of LoginFragment");
        super.onStart();
    }
    private void fetchSlider(String name) {
        client = new WebClient();

        client.getresult(name, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                old_button.setVisibility(View.INVISIBLE);
                popular_button.setVisibility(View.INVISIBLE);
                other_button.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(),"Something went wrong!",Toast.LENGTH_LONG).show();
                retry_button.setVisibility(View.VISIBLE);
                retry_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        retry_button.setVisibility(View.INVISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                        old_pub("old");
                        other_pub("other");
                        popular_pub("popular");
                        fetchSlider("latest");

                    }
                });


            }
            @Override
            public void onPreProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
               /* old_button.setVisibility(View.INVISIBLE);
                popular_button.setVisibility(View.INVISIBLE);
                other_button.setVisibility(View.INVISIBLE);*/

            }
            @Override
            public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {


            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                JSONArray item = null;

                try {
                    item = responseBody.getJSONArray("items");
                    //int values = responseBody.getInt("totalResults");
                    //video_id = new String[values];
                    //image_url = new String[values];

                    if (item.length() == 0) {

                    } else {
                        //searchResults = Modals.fromJson(item);
                        // Load model objects into the adapter
                        for (int i = 0; i < item.length(); i++) {
                            responseBody = item.getJSONObject(i);
                            String image = responseBody.getString("image");
                            String name = responseBody.getString("title");
                            String playid = responseBody.getString("id");
                            String trai = responseBody.getString("trailer");
                            int season = responseBody.getInt("Seasons");
                            if(season>1){
                                String second_id = responseBody.getString("id1");
                                slider_id2.add(second_id);
                            }else{
                                slider_id2.add(null);
                            }
                            slider_trailer.add(trai);
                            slider_id.add(playid);
                            slider_image.add(image);
                            slider_title.add(name);
                            Log.d("slider",image);

                            mCustomPagerAdapter.notifyDataSetChanged();


                        }
                        Log.d("items", item.length() + "of");


                    }

                } catch (JSONException e) {
                    Log.v("LOG_TAG", "id", e);
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.INVISIBLE);
                old_button.setVisibility(View.VISIBLE);
                old_text.setVisibility(View.VISIBLE);
                popular_button.setVisibility(View.VISIBLE);
                popular_text.setVisibility(View.VISIBLE);
                other_button.setVisibility(View.VISIBLE);
                other_text.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                retry_button.setVisibility(View.INVISIBLE);
            }


        });
    }


    private void other_pub(String name) {
        client = new WebClient();

        client.getresult(name, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                JSONArray item = null;

                try {
                    item = responseBody.getJSONArray("items");
                    //int values = responseBody.getInt("totalResults");
                    //video_id = new String[values];
                    //image_url = new String[values];

                    if (item.length() == 0) {

                    } else {
                        //searchResults = Modals.fromJson(item);
                        // Load model objects into the adapter
                        for (int i = 0; i < item.length(); i++) {
                            responseBody = item.getJSONObject(i);
                            String image = responseBody.getString("image");
                            String name = responseBody.getString("title");
                            String playid = responseBody.getString("id");
                            String trai = responseBody.getString("trailer");
                            int season = responseBody.getInt("Seasons");
                            Log.d("pub",season+"");
                            if(season>=2){
                                String second_id = responseBody.getString("id1");
                                other_id2.add(second_id);
                            }else{
                                other_id2.add(null);
                            }

                            other_trailer.add(trai);
                            other_id.add(playid);
                            other_image.add(image);
                            other_title.add(name);
                            Log.d("image",image);
                            other_Poster.notifyDataSetChanged();
                           // mPoster3.notifyDataSetChanged();


                        }
                        Log.d("items", item.length() + "of");

                    }

                } catch (JSONException e) {
                    Log.v("LOG_TAG", "id", e);
                    e.printStackTrace();
                }
            }
        });
    }
    private void popular_pub(String name) {
        client = new WebClient();

        client.getresult(name, new JsonHttpResponseHandler() {
            @Override
            public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {

            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                JSONArray item = null;

                try {
                    item = responseBody.getJSONArray("items");
                    //int values = responseBody.getInt("totalResults");
                    //video_id = new String[values];
                    //image_url = new String[values];

                    if (item.length() == 0) {

                    } else {
                        //searchResults = Modals.fromJson(item);
                        // Load model objects into the adapter
                        for (int i = 0; i < item.length(); i++) {
                            responseBody = item.getJSONObject(i);
                            String image = responseBody.getString("image");
                            String name = responseBody.getString("title");
                            String playid = responseBody.getString("id");
                            String trai = responseBody.getString("trailer");
                            int season = responseBody.getInt("Seasons");
                            if(season>1){
                                String second_id = responseBody.getString("id1");
                                popular_id2.add(second_id);
                            }else{
                                popular_id2.add(null);
                            }
                            popular_trailer.add(trai);
                            popular_id.add(playid);
                            popular_image.add(image);
                            popular_title.add(name);
                            Log.d("image55",image);
                            //mPoster.notifyDataSetChanged();
                            popular_Poster.notifyDataSetChanged();


                        }
                        Log.d("items", item.length() + "of");

                    }

                } catch (JSONException e) {
                    Log.v("LOG_TAG", "id", e);
                    e.printStackTrace();
                }
            }
        });
    }

    private void old_pub(String name) {
        client = new WebClient();

        client.getresult(name, new JsonHttpResponseHandler() {
            @Override
            public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {

            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                JSONArray item = null;

                try {
                    item = responseBody.getJSONArray("items");
                    //int values = responseBody.getInt("totalResults");
                    //video_id = new String[values];
                    //image_url = new String[values];

                    if (item.length() == 0) {

                    } else {
                        //searchResults = Modals.fromJson(item);
                        // Load model objects into the adapter
                        for (int i = 0; i < item.length(); i++) {
                            responseBody = item.getJSONObject(i);
                            String image = responseBody.getString("image");
                            String name = responseBody.getString("title");
                            String playid = responseBody.getString("id");
                            String trai = responseBody.getString("trailer");
                            int season = responseBody.getInt("Seasons");
                            if(season>1){
                                String second_id = responseBody.getString("id1");
                                old_id2.add(second_id);
                            }else{
                                old_id2.add(null);
                            }
                            old_trailer.add(trai);
                            old_id.add(playid);
                            old_image.add(image);
                            old_title.add(name);
                            Log.d("image55",image);
                            //mPoster.notifyDataSetChanged();
                            old_Poster.notifyDataSetChanged();


                        }
                        Log.d("items", item.length() + "of");

                    }

                } catch (JSONException e) {
                    Log.v("LOG_TAG", "id", e);
                    e.printStackTrace();
                }
            }
        });
    }
}
