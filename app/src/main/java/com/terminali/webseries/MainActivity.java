package com.terminali.webseries;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String type;
    String name;
    private FragmentManager mfragmentManager;
    private FragmentTransaction mfragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        View header = navigationView.getHeaderView(0);
        TextView textView = (TextView) header.findViewById(R.id.home);
        ImageView imageView =(ImageView) header.findViewById(R.id.logo);
        showSnack(isNetworkConnected());
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mfragmentManager = getSupportFragmentManager();
                mfragmentTransaction = mfragmentManager.beginTransaction();
                mfragmentTransaction.addToBackStack("click");
                mfragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
                mfragmentTransaction.replace(R.id.containerView, new Home_fragment(),"click").commit();

            }
        });
        mfragmentManager = getSupportFragmentManager();
        mfragmentTransaction = mfragmentManager.beginTransaction();
        mfragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
        mfragmentTransaction.replace(R.id.containerView, new Home_fragment(),"home").commit();

    }


    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }


    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("DEBUG", "onpause of LoginFragment");
    }

    @Override
    public void onResume() {
        super.onResume();

        showSnack(isNetworkConnected());
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
       /* if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getFragmentManager().popBackStack();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i  = new  Intent(this,Settings.class);
            startActivity(i);


            return true;
        }else if(id == R.id.feedback){
            mfragmentManager = getSupportFragmentManager();
            mfragmentTransaction = mfragmentManager.beginTransaction();
            mfragmentTransaction.addToBackStack("feedback");
            mfragmentTransaction.replace(R.id.containerView, new Feedback(),"feedback").commit();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_camera) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("key", "tvf");
            PublishersFrag publishersFrag = new PublishersFrag();
            publishersFrag.setArguments(bundle);
            fragmentTransaction.addToBackStack("tvf");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, publishersFrag,"tvf").commit();
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("key", "yrf");
            PublishersFrag y = new PublishersFrag();
            y.setArguments(bundle);
            fragmentTransaction.addToBackStack("yrf");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, y,"yrf").commit();

        } else if (id == R.id.nav_slideshow) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("key", "arre");
            PublishersFrag a = new PublishersFrag();
            a.setArguments(bundle);
            fragmentTransaction.addToBackStack("arre");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, a,"arre").commit();

        } else if (id == R.id.nav_share) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("key", "scoop");
            PublishersFrag sc = new PublishersFrag();
            sc.setArguments(bundle);
            fragmentTransaction.addToBackStack("scoop");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, sc,"scoop").commit();

        } else if (id == R.id.nav_send) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("key", "dice");
            PublishersFrag d = new PublishersFrag();
            d.setArguments(bundle);
            fragmentTransaction.addToBackStack("dice");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, d,"dice").commit();

        } else if (id == R.id.nav_about_us) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            //Bundle bundle = new Bundle();
            //bundle.putString("key","dice");
            Aboutme d = new Aboutme();
            //d.setArguments(bundle);
            fragmentTransaction.addToBackStack("about");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, d,"about").commit();

        } else if (id == R.id.bookmark) {

            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack("bookmark");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, new Bookmark(),"bookmark").commit();
        }else if (id == R.id.tbm) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("key", "tbm");
            PublishersFrag s = new PublishersFrag();
            s.setArguments(bundle);
            fragmentTransaction.addToBackStack("tbm");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, s,"tbm").commit();
    } else if (id == R.id.old) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("key", "old");
            PublishersFrag s = new PublishersFrag();
            s.setArguments(bundle);
            fragmentTransaction.addToBackStack("old");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, s,"old").commit();
        }else if (id == R.id.talkies) {
            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("key", "talkies");
            PublishersFrag s = new PublishersFrag();
            s.setArguments(bundle);
            fragmentTransaction.addToBackStack("talkies");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, s,"talkies").commit();
        }else if (id == R.id.home) {

            FragmentTransaction fragmentTransaction = mfragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack("home");
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left,R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.containerView, new Home_fragment(),"home").commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
