package com.terminali.webseries;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by TERMINALi on 10/28/2016.
 */

public class Modals {
    private String videoId;
    private String image_url;
    private String title;
    private String discription;

    public Modals() {

    }

    public Modals(String image_url, String videoId, String title, String discription) {
        this.image_url = image_url;
        this.videoId = videoId;
        this.title = title;
        this.discription = discription;
    }

    public String getVideoId() {
        return videoId;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getTitle() {
        return title;
    }

    public String getDiscription() {
        return discription;
    }


    public static Modals fromJson(JSONObject jsonObject) {
        Modals b = new Modals();
        try {

            b.image_url = jsonObject.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("standard").getString("url");
            b.videoId = jsonObject.getJSONObject("snippet").getJSONObject("resourceId").getString("videoId");
            Log.d("image_url", b.getImage_url());
        } catch (JSONException e) {
            e.printStackTrace();
            //return null;
        }
        return b;
    }


    public static ArrayList<Modals> fromJson(JSONArray jsonArray) {
        ArrayList<Modals> results = new ArrayList<Modals>(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject resultsJson = null;
            try {
                resultsJson = jsonArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                continue;
            }
            Modals modals = Modals.fromJson(resultsJson);
            Log.d("Added", "every");
            results.add(modals);
        }
        return results;
    }
}