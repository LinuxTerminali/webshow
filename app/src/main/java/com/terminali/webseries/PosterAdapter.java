package com.terminali.webseries;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by TERMINALi on 10/28/2016.
 */

public class PosterAdapter extends BaseAdapter {
    private final Context mContext;
    private  ArrayList<String> list=  new ArrayList<String>();
    /* public Integer[] mThumb = {
             R.drawable.tvf_tripling,R.drawable.sex_chat_with,
             R.drawable.baked,R.drawable.bang_baja,
             R.drawable.pitchers,R.drawable.official_chukyagiri,
             R.drawable.tanlines, R.drawable.mans_world
     };*/
    //ImageList list = new ImageList();
    private final ArrayList<String> name;


    public PosterAdapter(Context c, ArrayList<String> list, ArrayList<String> name) {
        mContext = c;
        this.list = list;
        this.name = name;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.main_adapter, parent, false);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.poster_image);
        TextView textView = (TextView) convertView.findViewById(R.id.name);
        textView.setText(name.get(position));
        /*imageView.setImageResource(mThumb[position]);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(100, 100));*/

        //Log.d("poster", name[position]);
        Picasso.with(mContext).load(list.get(position)).into(imageView);

        return convertView;
    }
}
