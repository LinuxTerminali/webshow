package com.terminali.webseries.Publishers;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;
import com.terminali.webseries.PublisherAdapter;
import com.terminali.webseries.R;
import com.terminali.webseries.Series_detail;
import com.terminali.webseries.WebClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBarUtils;
import fr.castorflex.android.smoothprogressbar.SmoothProgressDrawable;

public class Dice extends Fragment {
    ArrayList<String> imagep = new ArrayList<String>();
    ArrayList<String> title = new ArrayList<String>();
    ArrayList<String> play_id = new ArrayList<String>();
    ArrayList<String> trailer = new ArrayList<String>();
    ArrayList<String> id2 = new ArrayList<>();
    WebClient client;
    PublisherAdapter publisherAdapter;
    SmoothProgressBar progressBar;
    GridView gridView;
    public Dice() {

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        if(bundle!=null){
            String nam = bundle.getString("key");
            Log.d("name", nam);
        }
        super.onCreate(savedInstanceState);

    }

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_tvf, container, false);
            old_pub("dice");

            gridView = (GridView) view.findViewById(R.id.grid);
            progressBar = (SmoothProgressBar) view.findViewById(R.id.pocket);
            progressBar.setSmoothProgressDrawableBackgroundDrawable(
                    SmoothProgressBarUtils.generateDrawableWithColors(
                            getResources().getIntArray(R.array.pocket_background_colors),
                            ((SmoothProgressDrawable) progressBar.getIndeterminateDrawable()).getStrokeWidth()));
            progressBar.progressiveStart();
            progressBar.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.INVISIBLE);
            publisherAdapter = new PublisherAdapter(getActivity(), imagep, title);
            gridView.setAdapter(publisherAdapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getActivity(), Series_detail.class);
                    Bundle extras = new Bundle();

                    extras.putString("playlist", play_id.get(position));
                    extras.putString("name", title.get(position));
                    extras.putString("trailer", trailer.get(position));
                    extras.putString("image_url", imagep.get(position));
                    extras.putString("second_id", id2.get(position));
                    i.putExtras(extras);
                    //Toast.makeText(getActivity().this,"Clicked at"+type,Toast.LENGTH_SHORT).show();
                    startActivity(i);
                }
            });
        }
        return view;
    }
    private void old_pub(String name) {
        client = new WebClient();

        client.getresult(name, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progressBar.setVisibility(View.VISIBLE);
            }
            @Override
            public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {

            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseBody) {
                JSONArray item = null;

                try {
                    item = responseBody.getJSONArray("items");
                    //int values = responseBody.getInt("totalResults");
                    //video_id = new String[values];
                    //image_url = new String[values];

                    if (item.length() == 0) {

                    } else {
                        //searchResults = Modals.fromJson(item);
                        // Load model objects into the adapter
                        for (int i = 0; i < item.length(); i++) {
                            responseBody = item.getJSONObject(i);
                            String image = responseBody.getString("image");
                            String name = responseBody.getString("title");
                            String playid = responseBody.getString("id");
                            String trai = responseBody.getString("trailer");
                            int season = responseBody.getInt("Seasons");
                            if(season>1){
                                String second_id = responseBody.getString("id1");
                                id2.add(second_id);
                            }else{
                                id2.add(null);
                            }
                            imagep.add(image);
                            trailer.add(trai);
                            play_id.add(playid);

                            title.add(name);
                            Log.d("image55",image);
                            //mPoster.notifyDataSetChanged();
                            publisherAdapter.notifyDataSetChanged();


                        }
                        Log.d("items", item.length() + "of");

                    }

                } catch (JSONException e) {
                    Log.v("LOG_TAG", "id", e);
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.INVISIBLE);
                gridView.setVisibility(View.VISIBLE);
            }
        });
    }
}

