package com.terminali.webseries;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.terminali.webseries.Publishers.Arre;
import com.terminali.webseries.Publishers.Dice;
import com.terminali.webseries.Publishers.OldFrag;
import com.terminali.webseries.Publishers.Scoopwoop;
import com.terminali.webseries.Publishers.Sony;
import com.terminali.webseries.Publishers.Talkies;
import com.terminali.webseries.Publishers.Tbm;
import com.terminali.webseries.Publishers.Yrf;
import com.terminali.webseries.Publishers.tvf;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PublishersFrag extends Fragment {
    private String name ="";


    public PublishersFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        if(bundle!=null){
            name = bundle.getString("key");
            Log.d("name", name);
        }
        super.onCreate(savedInstanceState);
    }
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.publisher_fragment, container, false);
            ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
            setupViewPager(viewPager);
             getActivity().setTitle("Publisher");
            TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
        }
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        PublishersFrag.ViewPagerAdapter adapter = new PublishersFrag.ViewPagerAdapter(getChildFragmentManager());
        if(name.equals("tvf")) {
            adapter.addFragment(new tvf(), "TVF Shows");
        }else if(name.equals("yrf")){
            adapter.addFragment(new Yrf(), "YFilms Shows");
        }else if(name.equals("arre")){
            adapter.addFragment(new Arre(), "Arre Shows");
        }else if(name.equals("sony")){
            adapter.addFragment(new Sony(),  "SonyLiv Shows");
        }else  if(name.equals("scoop")){
            adapter.addFragment(new Scoopwoop(),"Scoopwhoop Shows");
        }else if(name.equals("dice")){
            adapter.addFragment(new Dice(),"Dice Media Shows");
        }else if(name.equals("tbm")){
            adapter.addFragment(new Tbm(),"TBM Shows");
        }else if(name.equals("talkies")){
            adapter.addFragment(new Talkies(),"Web Talkies Shows");
        }else if(name.equals("old")){
            adapter.addFragment(new OldFrag(),"Old Shows");
        }else if (name.equals("other")){
            adapter.addFragment(new OtherFrag(), "Trending Now");
        }
        Bundle bundle = new Bundle();
        bundle.putString("key",name);
        About about = new About();
        about.setArguments(bundle);
        adapter.addFragment(about, "About");
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            mFragmentTitleList.add("Latest Release");
            mFragmentTitleList.add("Top Rated");
            return mFragmentTitleList.get(position);
        }
    }
}

