package com.terminali.webseries;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SeasonDetaill extends Fragment {
String playid="";
    String second_id="";

    public SeasonDetaill() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        if(bundle!=null){
            playid  = bundle.getString("playid");
            second_id = bundle.getString("second_id");
            Log.d("play",playid);
        }
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.season_detail_frag, container, false);
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }


    private void setupViewPager(ViewPager viewPager){
        SeasonDetaill.ViewPagerAdapter adapter =  new SeasonDetaill.ViewPagerAdapter(getChildFragmentManager());
        Bundle bundle =  new Bundle();
        bundle.putString("playid",playid);
        bundle.putString("second_id",second_id);
        DetailFrag detailFrag = new DetailFrag();
        detailFrag.setArguments(bundle);
        DetailFragment detailFragment =  new DetailFragment();
        detailFragment.setArguments(bundle);
        adapter.addFragment(detailFragment,String.valueOf(Html.fromHtml("<strong>Season 01<strong>")));
        adapter.addFragment(detailFrag,"Season 02");
        viewPager.setAdapter(adapter);

    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            mFragmentTitleList.add("Season 1");
            mFragmentTitleList.add("Season 2");
            return mFragmentTitleList.get(position);
        }
    }
}
