package com.terminali.webseries;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.squareup.picasso.Picasso;

public class Series_detail extends AppCompatActivity implements YouTubeThumbnailView.OnInitializedListener{
    private String playid = "";
    private String name="";
    private  String trailer = "";
    private String image_url="";
    private String second_id="";
    YouTubeThumbnailView thumbnailView;
    YouTubeThumbnailLoader youTubeThumbnailLoader;
   String im ;
    private FragmentManager detailfragmanager;
    private FragmentTransaction detailfragtrans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_series_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        FrameLayout fragment = (FrameLayout) findViewById(R.id.containerView);
        //fragment.setBackgroundResource(R.drawable.aam_admi);
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        //String listName = extras.getString("list");
        trailer = extras.getString("trailer");
        playid = extras.getString("playlist");
        name = extras.getString("name");
        image_url=extras.getString("image_url");
        second_id= extras.getString("second_id");
        Bundle bundle = new Bundle();
        bundle.putString("playid",playid);
        bundle.putString("second_id",second_id);
        if(second_id==null) {
            //Toast.makeText(getApplicationContext(), "An error occurred", Toast.LENGTH_SHORT).show();
            detailfragmanager = getSupportFragmentManager();
            detailfragtrans = detailfragmanager.beginTransaction();
            DetailFragment detailFragment = new DetailFragment();
            detailFragment.setArguments(bundle);
            detailfragtrans.replace(R.id.containerView, detailFragment).commit();
        }else{
           // Toast.makeText(getApplicationContext(), second_id, Toast.LENGTH_SHORT).show();
            detailfragmanager = getSupportFragmentManager();
            detailfragtrans = detailfragmanager.beginTransaction();
            SeasonDetaill seasonDetaill = new SeasonDetaill();
            seasonDetaill.setArguments(bundle);
            detailfragtrans.replace(R.id.containerView, seasonDetaill).commit();
        }
        try {
            toolbar.setTitle(name);
            setSupportActionBar(toolbar);
            Log.d("trailer",trailer);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch(Exception e){
            Toast.makeText(getApplicationContext(),"An error occurred",Toast.LENGTH_SHORT).show();
        }
        //onBackPressed();
        //PosterAdapter posterAdapter = new PosterAdapter(this);
        thumbnailView = (YouTubeThumbnailView) findViewById(R.id.scroll_image);
        //imageView.setImageResource(posterAdapter.mThumb[position]);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Boolean j = preferences.getBoolean("youtube_player",false);
        if(j){
           // fetch("PL2BrJauH6oAQPPOIimPBaqNZel6TCwWRm");
            SharedPreferences preferenc = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            Boolean k = preferenc.getBoolean("image_res",false);
            if(k) {
                Picasso.with(getApplicationContext()).load("https://i.ytimg.com/vi/"+trailer+"/maxresdefault.jpg").error(R.drawable.default_film).resize(600, 600).into(thumbnailView);
            }else{
                Picasso.with(getApplicationContext()).load("https://i.ytimg.com/vi/"+trailer+"/hqdefault.jpg").error(R.drawable.default_film).resize(600, 600).into(thumbnailView);
            }
        }else {
            thumbnailView.initialize("AIzaSyCk_HAQ_PChKmTCzWFS8JCHvpre6Gt94UM", this);
        }
        thumbnailView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Youtube.class);
                Bundle extras = new Bundle();
                extras.putString("id", trailer);
                extras.putString("name", name);
                extras.putString("image_url",image_url);
                i.putExtras(extras);
                startActivity(i);
            }
        });



        showSnack(isNetworkConnected());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                if(second_id==null) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Watch Now " + name + " " + "https://www.youtube.com/playlist?list=" + playid + " for other series download WebShow now");
                }else{
                    shareIntent.putExtra(Intent.EXTRA_TEXT, "Watch Now "+ name+" S01 " +"https://www.youtube.com/playlist?list=" + playid + " and S02 "+" https://www.youtube.com/playlist?list=" + second_id +" for other series download WebShow now");
                }
                startActivity(shareIntent);
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        showSnack(isNetworkConnected());
    }
    @Override
    public void onStop() {
        super.onStop();
       // youTubeThumbnailLoader.release();

        }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (!isConnected) {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.fab2), message, Snackbar.LENGTH_LONG);

            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onInitializationFailure(YouTubeThumbnailView thumbnailView,
                                        YouTubeInitializationResult errorReason) {

        String errorMessage =
                String.format("Youtube app not found disable WebShow player from Setting",
                        errorReason.toString());
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubeThumbnailView thumbnailView,
                                        YouTubeThumbnailLoader thumbnailLoader) {

        /*Toast.makeText(getApplicationContext(),
                "onInitializationSuccess", Toast.LENGTH_SHORT).show();*/

        youTubeThumbnailLoader = thumbnailLoader;
        thumbnailLoader.setOnThumbnailLoadedListener(new ThumbnailListener());

        youTubeThumbnailLoader.setVideo(trailer);
    }

    private final class ThumbnailListener implements
            YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView thumbnail, String videoId) {
            /*Toast.makeText(getApplicationContext(),
                    "onThumbnailLoaded", Toast.LENGTH_SHORT).show();*/
        }

        @Override
        public void onThumbnailError(YouTubeThumbnailView thumbnail,
                                     YouTubeThumbnailLoader.ErrorReason reason) {
           /* Toast.makeText(getApplicationContext(),
                    "onThumbnailError", Toast.LENGTH_SHORT).show();*/
        }
    }


}
