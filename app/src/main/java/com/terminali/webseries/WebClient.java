package com.terminali.webseries;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

/**
 * Created by TERMINALi on 11/26/2016.
 */

public class WebClient {
    private AsyncHttpClient client;

    public WebClient() {

        this.client = new AsyncHttpClient();
    }

    private String getAPIurl(String relativeUrl) {
        String API_base_URL = "http://webshow.herokuapp.com/";
        return API_base_URL+relativeUrl;
    }

    public void getresult(String name, JsonHttpResponseHandler handler) {
         String url = getAPIurl(name);
        client.get(url, handler);
        //Log.d("complete_url",toString(client.get(url,params,handler)));

    }
}
