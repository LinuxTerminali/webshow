package com.terminali.webseries;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.ArrayList;
import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Youtube extends YouTubeBaseActivity {
    private String id = "";
    private String video_name ="";
    private String image_url="";
    private ImageView imageView;
    List<Datab> user_list;
    List<Datab> check;
    public   static  ArrayList<String> video_image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("user_list", "youtube crash");

        setContentView(R.layout.activity_youtube2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageView = (ImageView) findViewById(R.id.star);



        Intent i = getIntent();
        Bundle extras = i.getExtras();
        id = extras.getString("id");
        video_name = extras.getString("name");
        image_url = extras.getString("image_url");
        TextView textView = (TextView) findViewById(R.id.title);
        textView.setText(video_name);
        try{
             user_list = Datab.find(Datab.class, "VIDEOID=?", id);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"Something went wrong!",Toast.LENGTH_SHORT).show();
        }
        //Log.d("user_list", user_list.toString());
        try {
            if (user_list.size() > 0) {
                imageView.setImageResource(R.drawable.star_of);

            } else {
                imageView.setImageResource(R.drawable.star_on);
            }
        }catch(Exception e){
            Toast.makeText(getApplicationContext(),"Something went wrong!",Toast.LENGTH_SHORT).show();
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    check = Datab.find(Datab.class, "VIDEOID=?", id);
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Something went wrong!",Toast.LENGTH_SHORT).show();
                }
                if (check.size()>0) {
                    Datab.deleteAll(Datab.class,"VIDEOID=?",id);
                    imageView.setImageResource(R.drawable.star_on);
                    Toast.makeText(Youtube.this,"Bookmark removed",Toast.LENGTH_LONG).show();


                } else {
                    imageView.setImageResource(R.drawable.star_of);
                    Datab datab = new Datab(id,video_name,image_url);
                    datab.save();
                    //long f = datab.getId();
                    //Log.d("id",Long.toString(f));
                    Toast.makeText(Youtube.this,"Bookmark added",Toast.LENGTH_LONG).show();

                }
            }
        });
        YouTubePlayerView youTubePlayerView =
                (YouTubePlayerView) findViewById(R.id.player);

        youTubePlayerView.initialize("AIzaSyA1AeIzvDBltpD-tcsfphMF9ZelymiwOD0",
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {

                        // do any work here to cue video, play video, etc.
                        youTubePlayer.loadVideo(id);
                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });

    }




}
